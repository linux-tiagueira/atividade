#!/bin/bash

jogador_vida=$1
dragao_vida=$2

dragao_nome="Dragão"

while true; do
    clear
    echo "===== Jogo do Dragão ====="
    echo "Você: $jogador_vida pontos de vida"
    echo "$dragao_nome: $dragao_vida pontos de vida"
    echo "=========================="
    echo
    echo "Escolha uma opção:"
    echo "1 - Atacar"
    echo "2 - Fugir"
    echo "3 - Informações"
    echo

    read -p "Opção: " opcao
    echo

    case $opcao in
        1)
            echo "Você atacou o $dragao_nome e causou 100 pontos de dano."
            dragao_vida=$((dragao_vida - 100))

            echo "$dragao_nome contra-atacou e te causou 10 pontos de dano."
            jogador_vida=$((jogador_vida - 10))

            read -p "Pressione Enter para continuar..."
            ;;
        2)
            chance_fugir=$((RANDOM % 2))
            if [ $chance_fugir -eq 0 ]; then
                echo "Você fugiu com sucesso!"
                break
            else
                echo "Você foi derrotado pela baforada do $dragao_nome!"
                break
            fi
            ;;
        3)
            echo "Informações:"
            echo "Você: $jogador_vida pontos de vida"
            echo "$dragao_nome: $dragao_vida pontos de vida"
            read -p "Pressione Enter para continuar..."
            ;;
        *)
            echo "Opção inválida."
            read -p "Pressione Enter para continuar..."
            ;;
    esac
done

