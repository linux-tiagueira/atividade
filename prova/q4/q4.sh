#!/bin/bash

source funcoes.sh

nome_dragao="Dragão"

jogador_vida=$(gerar_numero_aleatorio 10 200)
dragao_vida=$(gerar_numero_aleatorio 100 5000)

jogador_vida=$((jogador_vida + 70))

while true; do
    clear
    exibir_menu
    echo

    read -p "Opção: " opcao
    echo

    case $opcao in
        1)
            atacar $nome_dragao
            ;;
        2)
            fugir $nome_dragao
            ;;
        3)
            curar $nome_dragao
            ;;
        4)
            exibir_informacoes $nome_dragao
            ;;
        *)
            echo "Opção inválida."
            ;;
    esac

    if [ $jogador_vida -le 0 ]; then
        fim_jogo "Você foi derrotado pelo $nome_dragao!"
        break
    elif [ $dragao_vida -le 0 ]; then
        fim_jogo "Você derrotou o $nome_dragao!"
        break
    fi

    ataque_dragao $nome_dragao
    echo
done | cowsay -f dragon

