#!/bin/bash

gerar_numero_aleatorio() {
    min=$1
    max=$2
    echo $((RANDOM % (max - min + 1) + min))
}

exibir_menu() {
    echo "===== Jogo do Dragão ====="
    echo "Você: $jogador_vida pontos de vida"
    echo "$nome_dragao: $dragao_vida pontos de vida"
    echo "=========================="
    echo
    echo "Escolha uma opção:"
    echo "1 - Atacar"
    echo "2 - Fugir"
    echo "3 - Curar"
    echo "4 - Informações"
    echo
}

atacar() {
    nome=$1
    dano=$(gerar_numero_aleatorio 10 100)
    dragao_vida=$((dragao_vida - dano))

    echo "Você atacou o $nome e causou $dano pontos de dano."

    for (( i=1; i<=5; i++ )); do
        ataque=$(gerar_numero_aleatorio 1 10)
        jogador_vida=$((jogador_vida - ataque))
        echo "$nome contra-atacou e te causou $ataque pontos de dano."
    done

    read -p "Pressione Enter para continuar..."
}

fugir() {
    nome=$1
    chance_fugir=$(gerar_numero_aleatorio 1 5)

    if [ $chance_fugir -eq 1 ]; then
        echo "Você fugiu com sucesso!"
        fim_jogo
    else
        echo "Você foi derrotado pela baforada do $nome!"
        fim_jogo
    fi
}

curar() {
    nome=$1
    cura=$(gerar_numero_aleatorio 50 100)
    jogador_vida=$((jogador_vida + cura))

    echo "Você se curou em $cura pontos de vida."

    chance_fugir_dragao=$(gerar_numero_aleatorio 1 10)
    if [ $chance_fugir_dragao -eq 1 ]; then
        echo "O $nome_dragao tentou fugir, mas não conseguiu."
    fi

    read -p "Pressione Enter para continuar..."
}

exibir_informacoes() {
    nome=$1
    echo "Informações:"
    echo "Você: $jogador_vida pontos de vida"
    echo "$nome: $dragao_vida pontos de vida"
    read -p "Pressione Enter para continuar..."
}

fim_jogo() {
    mensagem=$1
    echo
    echo "$mensagem"
    read -p "Pressione Enter para sair..."
    exit
}

