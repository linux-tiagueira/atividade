#!/bin/bash

lista_arquivos="lista.txt"
arquivo_resultado="resultado.txt"

while read -r arquivo; do
    hash=$(md5sum "$arquivo" | cut -d ' ' -f1)
    echo "$hash $arquivo" >> "$arquivo_resultado"
done < "$lista_arquivos"
