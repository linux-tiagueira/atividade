#!/bin/bash

while true; do
	clear
	echo "=== Menu==="
	echo "1 - veririficar se o usuario existe"
	echo "2- verificar se o usuario esta logado na maquina"
	echo "3- listar os arquivos da pasta home do usuario"
	echo "4 - sair"
	echo "-------------"
	echo

	read -p "escolha uma opçao" opcao
	echo

	case $opcao in
		1)
			read -p "digite o nome do usuario: "usuario
			id "$usuario" > /dev/null 2>&1
			if [ $? -eq 0 ]; then
				echo " o usuario $usuario existe."
			else
				echo " o usuario $usuario nao existe."
			fi
			;;
		2)
			read -p "digite o nome do usuario:" usuario
			who | grep -wq "$usuario"
			if [ $? -eq 0]; then
				echo "o usuario $usuario esta logado na maquina"
			else
				echo " o usuario $usuario nao esta logado na maquina"
			fi
			;;
		3)
			read -p "digite o nome do usuario: " usuario
			if id "$usuario" > /dev/null 2>&1; then
				echo "arquivos da pasta home do usuario $usuario:"
				ls -l "/home/$usuario"
			else
				echo "o usuario $usuario nao existe."
			fi
			;;
		4)
			echo "saindo "
			exit 0
			;;
		*)
			echo "opcao invalida."
			;;
	esac
	read -p "pressione enter para continuar.."
done
	
