a=$1
b=$2
op=$3

case $op in
    +) result=$((a + b));;
    -) result=$((a - b));;
    \*) result=$((a * b));;
    /) result=$((a / b));;
    **) result=$((a ** b));;
    *) echo "Operação inválida! Use +, -, *, / ou **"; exit 1;;
esac


echo "$a $op $b = $result"
