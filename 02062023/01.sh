#!/bin/bash

for dir in /etc /tmp; do
    for file in "$dir"/*; do
        if [ -d "$file" ]; then
            echo "$file é um diretório"
        elif [ -f "$file" ]; then
            if [ -x "$file" ]; then
                echo "$file é um arquivo executável"
            else
                echo "$file é um arquivo regular"
            fi
        elif [ -L "$file" ]; then
            echo "$file é um link simbólico"
        else
            echo "arquivo nao reconhecido: $file"
        fi
    done
done
