#!/bin/bash

num=$1
soma=0

for i in $(seq 1 $num); do
    soma=$((soma + i))
done

echo "A soma dos números de 1 até $num é: $soma"
