#!/bin/bash

while read site; do
  mkdir -p "$site"
  wget -P "$site" "$site"
done < sites.txt

#apaga os diretorios criados
while read site; do
  rm -rf "$site"
done < sites.txt
