#!/bin/bash

echo "Variaveis automaticas do bash:"
echo

echo "1. \%BASH_VERSION = $BASH_VERSION"
echo "2. \$HOME = $HOME"
echo "3. \$PWD = $PWD"
echo "4. \%OLDPWD = $OLDPWD"
echo "5. \$HOSTNAME = $HOSTNAME"
echo "6. \$UID = $UID"
echo "7. \$EUID = $EUID"
echo "8. \$SHELL = $SHELL"
echo "9. \$TERM = $TERM"
echo "10. \$HISTSIZE = $HISTSIZE"




