#!/bin/bash

var1="valor1"
var2=2
var3=3.14
var4=true

read -p "digite o valor da variavel 5: "var5
read -sp "digite agora o da 6(nao sera exibido): "var6
echo

var7= $(whoami)
var8=$(date +%Y-%m-%d)

var9="$1"
var10="$2"

echo "var1: $var1"
echo "var2: $var2"
echo "var3: $var3"
echo "var4: $var4"
echo "var5: $var5"
echo "var6: $var6"
echo "var7: $var7"
echo "var8: $var8"
echo "var9: $var9"
echo "var10: $var10"

echo
echo " diferença entre pedir explicitamente e receber como parametro de linha de comando:"
echo " se pedirmos explicitamente para o usuario digitar o valor de uma variavel, o script ira aguardar a entrada do usuario antes de continuar a execuçao. O valor digitado sera armazenado na variavel e podera ser usado posteriormente no script;"
echo " se recebermos o valor como parametro de linha de comando, o valor sera passado para o script no momento da sua invocaçao.Isso permite que o script seja executado automaticamente, sem interaçao com o usuario. O valor sera armazenado na variavel e podera ser usado posteriormente no script."

