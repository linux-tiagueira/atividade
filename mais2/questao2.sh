#!/bin/bash

next_wednesday=$(date -d "next wednesday" +%d/%m/%Y)
echo $next_wednesday
mkdir "backup$next_wednesday"


next_wednesday=$(date -d "next wednesday + 1 week" +%d/%m/%Y)
echo $next_wednesday
mkdir "backup$next_wednesday"


next_wednesday=$(date -d "next wednesday + 2 weeks" +%d/%m/%Y)
echo $next_wednesday
mkdir "backup$next_wednesday"


next_wednesday=$(date -d "next wednesday + 3 weeks" +%d/%m/%Y)
echo $next_wednesday
mkdir "backup$next_wednesday"

