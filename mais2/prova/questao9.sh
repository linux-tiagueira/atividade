#!/bin/bash

if [ $# -ne 2]; then
    echo "por favor passe 2 numeros como argumento."
    exit 1
fi

a=$1
b=$2

resultado=$(( (a+1) * (b+1) ))

echo $resultado
