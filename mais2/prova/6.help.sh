#!/bin/bash

echo " substituiçao de variaveis:"
echo "A substituiçao de variaveis é uma maneira de referenciar o valor de  uma variavel em um comando ou string."
echo "exemplo:"
nome="joao"
echo "Ola, $nome! seu nome tem ${#nome} letras."


echo " substituiçao de shell"
echo " A substituiçao de shell é uma maneira de executar um comando em um  subshell e usar a saida desse comando como entrada para outro comando ou variavel"
echo " exemplo:"
echo " o diretorio atual é $(pwd)"
echo " existem $(ls | wc -l) arquivos no diretorio atual"

echo " substituiçao aritmetica:"
echo " a substituiçao aritmetica é uma maneira de executar operaçoes aritmeticas em variaveis numericas e usar o resultado como entrada para outro comando ou variavel"
echo " exemplo"
x=10
y=5
echo " a soma de $x e $y é $((x+y))"

echo " boa leitura :)"



