#!/bin/bash

if [ $# -ne 2]; then
   echo "Este script precisa de 2 nomes de arquivos como argumento"
   exit 1
fi

arquivo1=$1
arquivo2=$2


total_linhas=$(($(wc -l < "$arquivo1") + $(wc -l <"$arquivo2") ))

echo "A soma das linhas nos arquivos é: $total_linhas"


