#!/bin/bash

if [[ $# -ne 2]]; then
    echo "Passe 2 numeros como arguimento"
    exit 1
fi

num1=$1
num2=$2

soma=$((num1+num2))

echo $soma
