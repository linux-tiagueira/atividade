#!/bin/bash

original_ps1=$PS1

echo "Escolha uma opção:"
echo "1 - Cor vermelha"
echo "2 - Cor verde"
echo "3 - Cor azul"
echo "4 - exibir data e hora"
echo "5 - retornar ao original"
read option

[ "$option" == "1" ] && PS1="\[\e[31m\]\u@\h:\w\$ \[\e[m\]"
[ "$option" == "2" ] && PS1="\[\e[32m\]\u@\h:\w\$ \[\e[m\]"
[ "$option" == "3" ] && PS1="\[\e[34m\]\u@\h:\w\$ \[\e[m\]"
[ "$option" == "4" ] && PS1="\[\e[33m\]\d \t \u@\h:\w\$ \[\e[m\]"
[ "$option" == "5" ] && PS1=$original_ps1
